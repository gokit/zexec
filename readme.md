ZExec
--------
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gokit/zexec)](https://goreportcard.com/report/gitlab.com/gokit/zexec)
[![Travis Build Status](https://travis-ci.org/gokit/zexec.svg?branch=master)](https://travis-ci.org/gokit/zexec#)
[![CircleCI](https://circleci.com/gh/gokit/zexec.svg?style=svg)](https://circleci.com/gh/gokit/zexec)

A simple shell execution library.

